# Elektroniczne Księgi Wieczyste

#### Opis działania:

Skrypt automatycznie oblicza cyfrę kontrolną po wprowadzeniu kodu wydziału i numeru księgi wieczystej na stronie [Wyszukiwanie Księgi Wieczystej](https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW) Ministerstwa Sprawiedliwości.

#### Instrukcja:

Do użycia skryptu niezbędne jest rozszerzenie przeglądarki umożliwiające dodawanie własnych skryptów (JavaScript) na stronach internetowych, np. [Custom Style Script](https://chrome.google.com/webstore/detail/custom-style-script/ecjfaoeopefafjpdgnfcjnhinpbldjij) (dla przeglądarek Chrome, Opera, Vivaldi i innych opartych na Chromium), lub [Custom Style Script](https://addons.mozilla.org/pl/firefox/addon/custom-style-script/) (dla przeglądarki Firefox).

Po zainstalowaniu rozszerzenia, otwieramy ustawienia (OPTIONS) i w polu **URL** wklejamy adres strony: `https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW`, a w polu **SCRIPT** wklejamy kod podany w pliku [script.js](/script.js). Zatwierdzamy klikając przycisk oznaczony symbolem: `+`

Działanie skryptu sygnalizowane jest zmianą koloru ramki pola cyfry kontrolnej na niebieski. W przypadku błędnego obliczenia cyfry kontrolnej (co jest raczej niemożliwe) można zawsze ręcznie wprowadzić jego wartość.

#### Uwagi:

W przypadku wypełnienia pola z numerem księgi wieczystej (środkowe pole) w pierwszej kolejności, a następnie wybrania kodu wydziału ręcznie z listy (pole po lewej) w drugiej kolejności, należy ponownie kliknąć w jedno (dowolne) z pól formularza, aby skrypt obliczył cyfrę kontrolną.

Jeżeli otworzymy kartę z przeglądarką "w tle", to po przejściu na nią skrypt może się nie załadować (ramka pola na cyfrę kontrolną nie zmieni koloru na niebieski). W takiej sytuacji należy odświeżyć stronę.