document.addEventListener("DOMContentLoaded", function() {
setTimeout(function() {
var lv=['X','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','Y','Z'],
    kodw=document.getElementById('kodWydzialuInput'),
    numkw=document.getElementById('numerKsiegiWieczystej'),
    cyfra=document.getElementById('cyfraKontrolna');
function calc(l) {
    for(i=0;i<lv.length;i++) {
        if(l==lv[i]) {
            return i+10; }
    }
}
function checksum() {
    var kw=kodw.value+numkw.value,sum;
    if (kw.length==12) {
        sum=1*calc(kw[0])+3*calc(kw[1])+7*kw[2]+1*calc(kw[3])+3*kw[4]+7*kw[5]+1*kw[6]+3*kw[7]+7*kw[8]+1*kw[9]+3*kw[10]+7*kw[11];
        sum %= 10;
        cyfra.value=sum; }
}
kodw.addEventListener('focusout', checksum);
numkw.addEventListener('focusout', checksum);
cyfra.addEventListener('focusin', checksum);
cyfra.setAttribute("style", "border: 1px solid #00408d");
},500);});